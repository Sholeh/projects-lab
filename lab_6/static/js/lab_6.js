var isSend = true;
var erase = false;

function sendMode(msg) {
    return '<div class="msg-send"><p>' + msg + '</p></div>';
}

function receiveMode(msg) {
    return '<div class="msg-receive"><p>' + msg + '</p></div>';
}

function postChat(msg) {
    var insertMsg = $('.msg-insert');
    if (isSend) {
        insertMsg.append(sendMode(msg));
    } else {
        insertMsg.append(receiveMode(msg));
    }
    isSend = !isSend;
}

var go = function (x) {
    var print = document.getElementById('print');
    if (x === 'ac') {
        print.value = '';
    } else if (x === 'eval') {
        print.value = Math.round(evil(print.value) * 10000) / 10000;
        erase = true;
    } else {
        print.value += x;
    }
};

function evil(fn) {
    return new Function('return ' + fn)();
}

function applyTheme(themeId) {
    var themes = JSON.parse(localStorage.getItem('themes'));
    selectedTheme = themes.find(theme => theme.id == themeId);
    $('body').css('background', selectedTheme.bcgColor);
    $('body').css('color', selectedTheme.fontColor);
    localStorage.setItem('selectedLastTheme', themeId);
}

function loadLastTheme() {
    selectedLastTheme = JSON.parse(localStorage.getItem('selectedLastTheme'));
    applyTheme(selectedLastTheme);
}

function initTheme() {
    if (localStorage.getItem('themes') === null) {
        themes = [{
                "id": 0,
                "text": "Red",
                "bcgColor": "#F44336",
                "fontColor": "#ABABAB"
            },
            {
                "id": 1,
                "text": "Pink",
                "bcgColor": "#E91E63",
                "fontColor": "#ABABAB"
            },
            {
                "id": 2,
                "text": "Purple",
                "bcgColor": "#9C27B0",
                "fontColor": "#ABABAB"
            },
            {
                "id": 3,
                "text": "Indigo",
                "bcgColor": "#3F51B5",
                "fontColor": "#ABABAB"
            },
            {
                "id": 4,
                "text": "Blue",
                "bcgColor": "#2196F3",
                "fontColor": "#212121"
            },
            {
                "id": 5,
                "text": "Teal",
                "bcgColor": "#009688",
                "fontColor": "#212121"
            },
            {
                "id": 6,
                "text": "Lime",
                "bcgColor": "#CDDC39",
                "fontColor": "#212121"
            },
            {
                "id": 7,
                "text": "Yellow",
                "bcgColor": "#FFEB3B",
                "fontColor": "#212121"
            },
            {
                "id": 8,
                "text": "Amber",
                "bcgColor": "#FFC107",
                "fontColor": "#212121"
            },
            {
                "id": 9,
                "text": "Orange",
                "bcgColor": "#FF5722",
                "fontColor": "#212121"
            },
            {
                "id": 10,
                "text": "Brown",
                "bcgColor": "#795548",
                "fontColor": "#ABABAB"
            }
        ];

        localStorage.setItem('themes', JSON.stringify(themes));
    }

    if (localStorage.getItem('selectedLastTheme') === null) {
        localStorage.setItem('selectedLastTheme', 0);
    }
}


$(document).ready(function () {
    initTheme();
    loadLastTheme();

    $('.my-select').select2({
        'data': JSON.parse(localStorage.getItem('themes')),
    }).select2("val", JSON.parse(localStorage.getItem('selectedLastTheme')));

    $('.apply-button').on('click', function () {
        applyTheme($('.my-select').val());
    });

    $('textarea').on('keydown', function (e) {
        if (e.keyCode == 13) {
            postChat($('textarea').val());
        }
    });
    
    $('textarea').on('keyup', function (e) {
        if (e.keyCode == 13) {
            $('textarea').val('');
        }
    });
});
