from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers

from .models import Friend
from .api_csui_helper.csui_helper import CSUIhelper
import os
import json
from django.forms.models import model_to_dict

response = {}

def index(request):
    csui_helper = CSUIhelper()
    # Page halaman menampilkan list mahasiswa yang ada
    # TODO berikan akses token dari backend dengan menggunakaan helper yang ada

    page = request.GET.get('page', '1')
    try:
        page = int(page)
        response_mahasiswa = csui_helper.instance.get_mahasiswa_list(page=page)
        mahasiswa_list = response_mahasiswa["results"]
    except Exception:
        return HttpResponseRedirect('/lab-7/')
    
    friend_list = Friend.objects.all()
    response = {"mahasiswa_list": mahasiswa_list, "friend_list": friend_list, "next_page":page+1, "prev_page":page-1}
    html = 'lab_7/lab_7.html'
    return render(request, html, response)

def friend_list(request):
    friend_list = Friend.objects.all()  
    response['friend_list'] = friend_list
    html = 'lab_7/friend_list.html'
    return render(request, html, response)

def get_friend_list(request):
    data = [model_to_dict(friend) for friend in Friend.objects.all()]
    return JsonResponse({'datas': data})

@csrf_exempt
def add_friend(request):
    if request.method == 'POST':
        name = request.POST['name']
        npm = request.POST['npm']
        is_exist = npm_is_exist(npm)
        if (not is_exist):
            friend = Friend(friend_name=name, npm=npm)
            friend.save()
            data = model_to_dict(friend)
            return JsonResponse(data)
        return JsonResponse({'message': 'mahasiswa sudah ditambahkan sebagai teman'}, status=400)
        
@csrf_exempt    
def delete_friend(request, friend_id):
    Friend.objects.filter(id=friend_id).delete()
    return HttpResponse()

@csrf_exempt
def validate_npm(request):
    npm = request.POST.get('npm', None)
    is_exist = npm_is_exist(npm);
    data = {
        'is_taken': is_exist #lakukan pengecekan apakah Friend dgn npm tsb sudah ada
    }
    return JsonResponse(data)

def npm_is_exist(npm):
    return (Friend.objects.filter(npm=npm).count() != 0)